# 从零开始手把手教你实现一个简单的RPC框架

- Java版本：21
- Spring Boot版本：3.2.2

> 注：proxy中的CGLIB 动态代理最高支持到Java 8，所以这部分实验无法在Java 21环境下正常运行！