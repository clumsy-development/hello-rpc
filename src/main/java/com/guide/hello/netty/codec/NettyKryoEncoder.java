package com.guide.hello.netty.codec;

import com.guide.hello.netty.serialize.Serializer;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import lombok.AllArgsConstructor;

/**
 * 自定义编码器：网络传输需要通过字节流来实现，ByteBuf可以看作是Netty提供的字节数据的容器，使用它会让我们更加方便地处理字节数据。
 */
@AllArgsConstructor
public class NettyKryoEncoder extends MessageToByteEncoder<Object> {
    private final Serializer serializer;
    private final Class<?> genericClass;

    /**
     * 将对象转换为字节码然后写入到ByteBuf对象中
     */
    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, Object object, ByteBuf byteBuf) {
        if (genericClass.isInstance(object)) { // 检查object是否是genericClass的实例或子类实例
            // 1. 将对象转换为byte数组
            byte[] body = serializer.serialize(object);
            // 2. 读取消息的长度
            int dataLength = body.length;
            // 3. 写入消息对应的字节数组长度，writerIndex加4
            byteBuf.writeInt(dataLength);
            // 4. 将字节数组写入ByteBuf对象中
            byteBuf.writeBytes(body);
        }
    }
}