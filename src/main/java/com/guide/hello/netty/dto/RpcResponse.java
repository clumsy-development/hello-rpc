package com.guide.hello.netty.dto;

import lombok.*;

// 服务端响应实体类
@AllArgsConstructor
@Getter
@NoArgsConstructor
@Builder
@ToString
public class RpcResponse {
    private String message;
}