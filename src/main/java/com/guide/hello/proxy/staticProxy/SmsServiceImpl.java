package com.guide.hello.proxy.staticProxy;

public class SmsServiceImpl implements SmsService {
    public void send(String message) {
        System.out.println("send message: " + message);
    }
}