package com.guide.hello.proxy.staticProxy;

public interface SmsService {
    void send(String message);
}