package com.guide.hello.proxy.dynamicProxy.cglibDynamicProxy;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class DebugMethodInterceptor implements MethodInterceptor { // 自定义MethodInterceptor（方法拦截器）
    /**
     * @param object      代理对象（增强的对象）
     * @param method      被拦截的方法（需要增强的方法）
     * @param args        方法入参
     * @param methodProxy 用于调用原始方法
     */
    @Override
    public Object intercept(Object object, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        //调用方法之前，我们可以添加自己的操作
        System.out.println("before method " + method.getName());

        Object o = methodProxy.invokeSuper(object, args);

        //调用方法之后，我们同样可以添加自己的操作
        System.out.println("after method " + method.getName());
        return o;
    }
}
