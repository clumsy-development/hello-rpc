package com.guide.hello.proxy.dynamicProxy.cglibDynamicProxy;

public class AliSmsService { // 使用阿里云发送短信的类
    public void send(String message) {
        System.out.println("send message: " + message);
    }
}