# CGLIB不支持Java 8以上的版本！

尝试使用CGLIB库创建一个动态代理，但是遇到了Java模块系统（Java Platform Module System，JPMS）的访问限制。从Java 9开始，引入了模块系统来封装类库并控制它们之间的访问。

错误 `java.lang.reflect.InaccessibleObjectException`表示你试图通过反射访问一个不应该被访问的成员（在这个例子中是`ClassLoader`类的`defineClass`方法）。

这是因为`defineClass`方法是`protected final`的，并且属于`java.base`模块，该模块并没有对你的代码所在的模块开放。

要解决这个问题，有几个选项。

## 添加VM参数

你可以在运行Java程序时添加一个VM参数来允许反射访问。对于你遇到的情况，你需要添加`--add-opens`参数来开放`java.base`模块中的`java.lang`包：

```bash
java --add-opens java.base/java.lang=ALL-UNNAMED -jar your-app.jar
```

这个参数告诉JVM允许所有未命名模块访问`java.base`模块中的`java.lang`包。注意，这可能会带来安全风险，因为它允许绕过模块的封装。

## 使用其他代理库

如果可能的话，考虑使用Java内置的`java.lang.reflect.Proxy`或者其他不需要反射访问`ClassLoader`内部方法的代理库。

## 回到Java 8或更早版本

如果你不需要Java 9或更高版本的新特性，并且这个问题对你的项目来说是阻塞性的，你可以考虑将项目降级到Java 8或更早的版本，这些版本没有模块系统的限制。

请注意，选择哪种解决方案取决于你的具体需求和环境。如果可能的话，避免通过反射绕过模块系统的限制，因为这可能会对你的应用程序的安全性和稳定性造成影响。