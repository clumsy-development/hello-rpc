package com.guide.hello.proxy.dynamicProxy.jdkDynamicProxy;

public class SmsServiceImpl implements SmsService { // 实现发送短信的接口
    public void send(String message) {
        System.out.println("send message: " + message);
    }
}