package com.guide.hello.proxy.dynamicProxy.jdkDynamicProxy;

public interface SmsService { // 定义发送短信的接口
    void send(String message);
}