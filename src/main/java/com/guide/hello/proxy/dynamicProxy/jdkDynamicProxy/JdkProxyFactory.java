package com.guide.hello.proxy.dynamicProxy.jdkDynamicProxy;

import java.lang.reflect.Proxy;

public class JdkProxyFactory { // 获取代理对象的工厂类
    public static Object getProxy(Object target) {
        return Proxy.newProxyInstance(
                target.getClass().getClassLoader(),
                target.getClass().getInterfaces(),
                new DebugInvocationHandler(target)
        );
    }
}
